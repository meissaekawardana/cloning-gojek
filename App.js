import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Image, StyleSheet, Text, TextInput, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.wrapperSearchBar}>
          {/* Search bar */}
          <View style={styles.wrapperSearch}>
            {/* search */}
              <TextInput placeholder="What do you want to eat?" style={styles.searchBarStyle}/>
              <Image source={require('./assets/icons/search.png')} style={styles.iconSearch}/>
          </View> 
          {/* Promo */}
          <View style={styles.wrapperPromo}>
            <Image source={require('./assets/icons/kupon.png')} style={styles.iconPromo}/>
          </View>
        </View>
        
        {/* Gopay */}
        <View style={styles.wrapperGopay}>
          {/* header */}
          <View style={styles.wrapperGopayHeader}>
            {/* wrapper header */}
              <Image source={require('./assets/icons/gopay.png')} style={styles.iconGopay} />
              <Text style={styles.textGopay}>
                Rp2.000.000
              </Text>
          </View>
            {/* wrapper Content */}
          <View style={styles.wrapperGopayBody}>
            <View style={styles.wrapperIconBodyGopay}>
              <Image source={require('./assets/icons/pay.png')} style={styles.iconBodyGopay}></Image>
              <Text style={styles.textGopayBody}>Pay</Text>
            </View>
            <View style={styles.wrapperIconBodyGopay}>
              <Image source={require('./assets/icons/nearby.png')} style={styles.iconBodyGopay}></Image>
              <Text style={styles.textGopayBody}>Nearby</Text>
            </View>
            <View style={styles.wrapperIconBodyGopay}>
              <Image source={require('./assets/icons/topup.png')} style={styles.iconBodyGopay}></Image>
              <Text style={styles.textGopayBody}>Top Up</Text>
            </View>
            <View style={styles.wrapperIconBodyGopay}>
              <Image source={require('./assets/icons/more.png')} style={styles.iconBodyGopay}></Image>
              <Text style={styles.textGopayBody}>More</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.menuBar}>
        <View style={styles.home}>
          <View style={styles.imageIcon}>
            <Image source={require('./assets/icons/home_active.png')} style={styles.icon}></Image>
          </View>
          <Text style={styles.menuTextActive}>Home</Text>
        </View>
        <View style={styles.home}>
          <View style={styles.imageIcon}>
            <Image source={require('./assets/icons/order.png')} style={styles.icon}></Image>
          </View>
          <Text style={styles.menuText}>Orders</Text>
        </View>
        <View style={styles.home}>
          <View style={styles.imageIcon}>
            <Image source={require('./assets/icons/help.png')} style={styles.icon}></Image>
          </View>
          <Text style={styles.menuText}>Help</Text>
        </View>
        <View style={styles.home}>
          <View style={styles.imageIcon}>
            <Image source={require('./assets/icons/inbox.png')} style={styles.icon}></Image>
          </View>
          <Text style={styles.menuText}>Inbox</Text>
        </View>
        <View style={styles.home}>
          <View style={styles.imageIcon}>
            <Image source={require('./assets/icons/account.png')} style={styles.icon}></Image>
          </View>
          <Text style={styles.menuText}>Account</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:'pink',
    flex:1
  },
  content:{
    flex:1,
    backgroundColor:'white'
  },
  menuBar:{
    height:54,
    backgroundColor:'white',
    flexDirection:'row',
    alignContent:'space-around',
    textAlign:'center'
  },
  home:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'white',
    marginTop:4
  },
  imageIcon:{
    width:26,
    height:26,
    backgroundColor:'pink'
  },
  icon:{
    width:26,
    height:26
  },
  menuText:{
    color:'#545454',
  },
  menuTextActive:{
    color:'#43ab4a'
  },
  searchBarStyle:{
    borderWidth:1,
    borderColor:'#e8e8e',
    borderRadius:25,
    height:40,
    fontSize:13,
    paddingLeft : 45,
    paddingRight : 20,
    backgroundColor:'white'
  },
  iconSearch:{
    position:'absolute',
    top:8,
    left:15,
    width:23,
    height:23,
    marginRight:18
  },
  iconPromo:{
    width:27,
    height:27
  },
  wrapperSearchBar:{
    marginHorizontal:17,
    flexDirection:'row',
    paddingTop:15
  },
  wrapperSearch:{
    position:'relative', 
    flex:1
  },
  wrapperPromo:{
    width:35,    
    alignItems:'center',
    justifyContent:'center',
    marginLeft:8
  },
  wrapperGopay:{
    marginTop:10,
    marginHorizontal:17,
    backgroundColor:'#0058c2',
    height:170,
    borderRadius:10,
    // position:'relative'
  },
  wrapperGopayHeader:{
    flexDirection:'row',
    justifyContent:'space-between',
    backgroundColor:'#245096',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
  },
  iconGopay:{
    width:135,
    height:30,
    marginLeft:25,
    marginTop:15,
    marginBottom:15
  },
  textGopay:{
    color:'white',
    fontWeight:'bold',
    fontSize:30,
    marginTop:7,
    marginRight:20,
  },
  wrapperGopayBody:{
    flexDirection:'row',
    flex:1,
    justifyContent:'space-between',
    paddingHorizontal:10
  },
  wrapperIconBodyGopay:{
    backgroundColor:'pink',
    flex:1,
    alignItems:'center',
    paddingTop:15,
    paddingBottom:15,
    backgroundColor:'#0058c2'
  },
  iconBodyGopay:{
    width:50,
    height:50
  },
  textGopayBody:{
    color:'white',
    fontWeight:'500',
    fontSize:13,
    marginTop:8
  }
});
